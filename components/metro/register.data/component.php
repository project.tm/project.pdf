<?php

use Tools\Constants;
use Tools\Helpers;


if ( ! defined( "B_PROLOG_INCLUDED" ) || B_PROLOG_INCLUDED !== true ) {
	die();
}
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
require($_SERVER['DOCUMENT_ROOT'].'/local/form_pdf/Library/NCL.NameCase.ru.php');  //Подключаем либу для скролонения ФИО
require($_SERVER['DOCUMENT_ROOT'].'/local/php_interface/vendor/autoload.php');

if(empty($arParams['ID'])) {
    $rsUser    = CUser::GetByID( $USER->GetID() );
    $arUser    = $rsUser->Fetch();
    $partnerID = $arUser['UF_PARTNER'];
} else {
    foreach (\Local\Helpers\Applications::getDataPgf(array('ID' => $arParams['ID'])) as $arItem) {
    }
    $partnerID = $arItem['PARTNER'];
    $arItem['sex_man']   = ( $arItem['sex'] == 'man' ) ? 'checked' : '';
    $arItem['sex_woman'] = ( $arItem['sex'] == 'woman' ) ? 'checked' : '';
    $arResult = $arItem;
    if(empty($arItem['NUM_CART'])) {
        $arParams['isEmptyCard'] = true;
    }
//    pre($arResult);
}

if(!CModule::IncludeModule('iblock')) {
    echo ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
    exit;
}

$arFilter = [
    'IBLOCK_ID' => Constants::$IBLOCK_PARTNERS,
    'ID' => $partnerID
];
$arSelect = Array("PROPERTY_IS_CARD_LOYALTY", 'PROPERTY_REGISTER_SMALL');
$res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
$arPartner = $res->Fetch();
$isCardLoyalty = $arPartner['PROPERTY_IS_CARD_LOYALTY_VALUE'] === 'Y';
$isRegisterSmall = $arPartner['PROPERTY_REGISTER_SMALL_VALUE'] === 'Y';
if($isRegisterSmall and empty($arParams['ID'])) {
    if($APPLICATION->GetCurPage(false)==='/register/manager/') {
        LocalRedirect('/register/');
    }
    $arParams['isManager'] = \Local\Manager::REGISTER_SMALL;
}

$uploadDir = $_SERVER["DOCUMENT_ROOT"] .'/upload/pdf/register.data/';

if (!empty($partnerID)) {

	if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
        ignore_user_abort(true);
        set_time_limit(0);
		$sex_man   = ( $_REQUEST['sex'] == 'man' ) ? 'checked' : '';
		$sex_woman = ( $_REQUEST['sex'] == 'woman' ) ? 'checked' : '';
		$errors    = false;
		$arResult  = [
			'sex_man'   => $sex_man,
			'sex_woman' => $sex_woman,
            'sex'       => Helpers::secureStr($_REQUEST['sex']),
			'fname'     => Helpers::secureStr($_REQUEST['fname']),
			'mname'     => Helpers::secureStr($_REQUEST['mname']),
			'lname'     => Helpers::secureStr($_REQUEST['lname']),
			'birthday'     => Helpers::secureStr($_REQUEST['birthday']),
			'email'     => Helpers::secureStr($_REQUEST['email']),
			'postcode'     => Helpers::secureStr($_REQUEST['postcode']),
			'region'     => Helpers::secureStr($_REQUEST['region']),
			'district'     => Helpers::secureStr($_REQUEST['district']),
			'city'     => Helpers::secureStr($_REQUEST['city']),
			'street'     => Helpers::secureStr($_REQUEST['street']),
			'number-house'     => Helpers::secureStr($_REQUEST['number-house']),
			'housing'     => Helpers::secureStr($_REQUEST['housing']),
			'number-flat'     => Helpers::secureStr($_REQUEST['number-flat']),
			'agree'     => Helpers::secureStr($_REQUEST['agree']),
			'agree1'     => Helpers::secureStr($_REQUEST['agree1']),
		];
        if($isCardLoyalty) {
            $arResult['card_loyalty'] = Helpers::secureStr($_REQUEST['card_loyalty']);
        }

		if ( $_REQUEST['fname'] == '' || $_REQUEST['mname'] == '' || $_REQUEST['lname'] == '' ) {
			$arResult['ERRORS_FIELD'][] = 'Поля "Фамилия", "Имя", "Отчество" обязательны для заполнения';
			$errors                     = true;
		}
		if(preg_match('|^[A-Z0-9]+$|i',$_POST['fname']) || preg_match('|^[A-Z0-9]+$|i',$_POST['mname']) || preg_match('|^[A-Z0-9]+$|i',$_POST['lname'])) {
                    $arResult['ERRORS_FIELD'][] = 'Поля  "Фамилия", "Имя", "Отчество" имеют недопустимые символы';
                    $errors                     = true;
        }

		if ( $_REQUEST['email'] == '' ) {
			$arResult['ERRORS_FIELD'][] = 'Поле "Email" обязательно для заполнения';
			$errors                     = true;
		} else
		if (!check_email($_REQUEST['email'])) {
			$arResult['ERRORS_FIELD'][] = 'Поле "Email" заполнено не правильно';
			$errors                     = true;
		}

        if(empty($arParams['isManager'])) {
            $arEnValid = array(
                'region' => 'Область',
                'district' => 'Район',
                'city' => 'Город',
                'street' => 'Улица',
            );
            foreach ($arEnValid as $key => $value) {
                if(preg_match('|^[A-Z0-9]+$|i',$_POST[$key])) {
                            $arResult['ERRORS_FIELD'][] = 'Поле "'. $value .'" имеют недопустимые символы';
                            $errors                     = true;
                }
            }

            if ( $_REQUEST['birthday'] == '' ) {
                $arResult['ERRORS_FIELD'][] = 'Поле "Дата рождения" обязательно для заполнения';
                $errors                     = true;
            }

            if($isCardLoyalty) {
                if ( $_REQUEST['card_loyalty'] == '' ) {
                    $arResult['ERRORS_FIELD'][] = 'Поле "EY Alumni Card Number" обязательно для заполнения';
                    $errors                     = true;
                } elseif(preg_match('|^[^0-9]+$|i',$_POST['card_loyalty'])) {
                    $arResult['ERRORS_FIELD'][] = 'Поле "EY Alumni Card Number" имеют недопустимые символы';
                    $errors                     = true;
                }
            }

            if ( $_REQUEST['postcode'] == '' || $_REQUEST['city'] == '' || $_REQUEST['street'] == '' || $_REQUEST['number-house'] == '' ) {
                $arResult['ERRORS_FIELD'][] = 'Поля "Почтовый индекс", "Город", "Улица", "№ дома" обязательны для заполнения';
                $errors                     = true;
            }

            if ( ! isset( $_REQUEST['agree'] ) ) {
                $arResult['ERRORS_FIELD'][] = 'Вы не дали согласие на обработку персональных данных';
                $errors                     = true;
            }

            if ( ! isset( $_REQUEST['agree1'] ) ) {
                        $arResult['ERRORS_FIELD'][] = 'Вы не подписались на получение специального предложения со скидками на первую покупку';
                        $errors                     = true;
                    }
        }

		if ( !$errors ) {
			if ( CModule::IncludeModule( 'iblock' ) ) {
                $ibElement = new CIBlockElement();

                // check for existing application
                if(empty($arParams['ID'])) {
                    $exist = $ibElement->GetList([], [
                        '=IBLOCK_ID'      => Tools\Constants::$IBLOCK_APPLICATIONS,
                        '=PROPERTY_EMAIL' => $arResult['email']
                    ])->Fetch();
                    if ($exist) {
                        $arResult['ERRORS_FIELD'][] = 'Заявка с данным E-mail уже существует';
                        $errors                     = true;
                    }
                } else {
                    $exist = $arItem['email'] != $arResult['email'];
                    if ($exist) {
                        $arResult['ERRORS_FIELD'][] = 'Вы не можите изменить E-mail';
                        $errors                     = true;
                    }
                }

                if ($exist) {
                } else {
//                    $DB->Query('SET SESSION TRANSACTION ISOLATION LEVEL SERIALIZABLE;');
                    $DB->StartTransaction();
                    if(empty($arParams['ID']) or isset($arParams['isEmptyCard'])) {
                        $arFilter = [
                            'IBLOCK_ID'        => Constants::$IBLOCK_CARDS,
                            'ACTIVE'           => 'Y',
                            'PROPERTY_PARTNER' => $partnerID
                        ];
                        $cards = $ibElement->GetList( ['rand'=> 'asc'], $arFilter, false, ["nTopCount"=>15], array('ID', 'NAME') );
                        $count_cart=$cards->SelectedRowsCount();
                    } else {
                        $count_cart = 0;
                    }

                    if($count_cart>0) {
                        $arCardList = array();
                        while($card = $cards->Fetch()) {
                            $arCardList[] = $card;
                        }
                        $card = $arCardList[array_rand($arCardList)];
                        $ibElement->Update($card['ID'], ['ACTIVE' => 'N']);
                        $DB->Commit();
                    } else {
                        $DB->Rollback();
                    }

                    if($count_cart>0) {
                        if ($count_cart <= 10) {
                            $arFilter = [
                                'IBLOCK_ID' => Constants::$IBLOCK_PARTNERS,
                                'ID' => $partnerID
                            ];
                            $arSelect = Array("ID", "NAME", "PROPERTY_E_MAIL");

                            $partners = $ibElement->GetList([], $arFilter, false, $arSelect);
                            while ($ar = $partners->GetNext()) {
                                $partner_name = $ar["NAME"];

                                $db_props = $ibElement->GetProperty(Constants::$IBLOCK_PARTNERS, $ar["ID"],
                                    array("sort" => "asc"), Array("CODE" => "E_MAIL"));
                                if ($ar_props = $db_props->Fetch()) {
                                    $partner_email = $ar_props["VALUE"];
                                } else {
                                    $partner_email = '';
                                } //Дефолтовое мыло
                            }
                            
                            $arEventFields_warning = array(
                                'BASE_NAME' => $partner_name,
                                'COUNT_CART' => $count_cart,
                                'EMAIL' => $partner_email
                            );
                            $event_warning = new CEvent;
                            $event_warning->SendImmediate("WARNING_MESSAGE", 's1', $arEventFields_warning, "N", 9);
//                            pre('WARNING_MESSAGE', $arEventFields_warning);
                        }

//                        $card = $cards->Fetch();
                        $arResult['NUM_CART'] = $card['NAME'];

                        $first = urlencode($arResult['fname']);
                        $middle = urlencode($arResult['mname']);
                        $last = urlencode($arResult['lname']);
                        $arFormParams = array(
                            'NUM_CART',
                            'sex',
                            'fname',
                            'mname',
                            'lname',
                            'birthday',
                            'email',
                            'postcode',
                            'region',
                            'district',
                            'city',
                            'street',
                            'number-house',
                            'housing',
                            'number-flat',
                            'card_loyalty',
                        );
                        $arFormValue = array();
                        foreach ($arFormParams as $key) {
                            $arFormValue[$key] = isset($arResult[$key]) ? ($arResult[$key]) : '';
                        }
                        $res = \Local\Cards::generatePdf($arFormValue);
//file_put_contents($_SERVER['DOCUMENT_ROOT'].'/pdf.log', '========================'.PHP_EOL.PHP_EOL.'PDF:'.($res ? 'true':'false'),FILE_APPEND);
//                        $srt = dirname(__FILE__) . "/rastr.js";
//                        $str_exec = Constants::$PHANTOM_DIR . " $srt 'http://{$_SERVER['SERVER_NAME']}/local/form_pdf/form.php?" . http_build_query($arFormValue) . "' " . dirname(__FILE__) . "/card_" . $card['NAME'] . ".pdf 2>&1";
//                        //                    $str_exec = Constants::$PHANTOM_DIR." $srt 'http://{$_SERVER['SERVER_NAME']}/local/form_pdf/form.php?NUM_CART={$card['NAME']}&FAM={$first}&IM={$middle}&OT={$last}&POL={$arResult['sex']}' ".dirname(__FILE__)."/card_".$card['NAME'].".pdf 2>&1";
//                        $res = exec($str_exec);
//                        echo $res;
                        if ($res) {
//                            $ibElement->Update($card['ID'], ['ACTIVE' => 'N']);
//file_put_contents($_SERVER['DOCUMENT_ROOT'].'/pdf.log', PHP_EOL.'inside if...',FILE_APPEND);
                            $arrPrp = [
                                'LAST_NAME' => $arResult['lname'],
                                'PARTNER' => $partnerID,
                                'CARD' => $card['ID'],
                                'EMAIL' => $arResult['email'],
                                'NAME' => $arResult['fname'],
                                'MID_NAME' => $arResult['mname'],
                                'SEX' => $arResult['sex'],
                                'BIRTHDAY' => $DB->FormatDate($arResult['birthday'], "DD/MM/YYYY", "DD.MM.YYYY"),
                                'POSTCODE' => $arResult['postcode'],
                                'REGION' => $arResult['region'],
                                'DISTRICT' => $arResult['district'],
                                'CITY' => $arResult['city'],
                                'STREET' => $arResult['street'],
                                'NUMBER_HOUSE' => $arResult['number-house'],
                                'HOUSING' => $arResult['housing'],
                                'NUMBER_FLAT' => $arResult['number-flat'],
                            ];
                            if($isCardLoyalty) {
                                $arrPrp['CARD_LOYALTY'] = $arResult['card_loyalty'];
                            }
                            $arrFields = [
                                'IBLOCK_ID' => Constants::$IBLOCK_APPLICATIONS,
                                'NAME' => $arResult['email'],
                                'ACTIVE' => 'Y',
                                'PROPERTY_VALUES' => $arrPrp
                            ];

                            if(empty($arParams['ID'])) {
//file_put_contents($_SERVER['DOCUMENT_ROOT'].'/pdf.log', PHP_EOL.'empty case',FILE_APPEND);
                                $orderId = $res = $ibElement->Add($arrFields);
                            } else {
//file_put_contents($_SERVER['DOCUMENT_ROOT'].'/pdf.log', PHP_EOL.'NOT empty case (else)',FILE_APPEND);
                                foreach ($arrFields['PROPERTY_VALUES'] as $key => $value) {
                                    CIBlockElement::SetPropertyValues($arParams['ID'], Constants::$IBLOCK_APPLICATIONS, $value, $key);
                                }
                                $ibElement->Update($arItem['ID'], ['CODE' => '']);
                                $res = true;
                            }
//file_put_contents($_SERVER['DOCUMENT_ROOT'].'/pdf.log',PHP_EOL.'res:'.($res ? 'true':'false'),FILE_APPEND);
                            if ($res) {
                                /**
                                 * Создаем обьект класса.
                                 * Теперь библиотека готова к работе
                                 */
//                                $nc = new NCLNameCaseRu();
//                                /**
//                                 * Производим склонения и выводим результат на экран
//                                 */
//                                $fio = $nc->q($arResult['mname'] . " " . $arResult['lname']);
//
//                                if ($arResult['sex'] == "man") {
//                                    $treatment = "Уважаемый " . $fio[0] . "!";
//                                }
//                                if ($arResult['sex'] == "woman") {
//                                    $treatment = "Уважаемая " . $fio[0] . "!";
//                                }
                                // файл, который будет приложен к письму
//                                $arFile = CFile::MakeFileArray(dirname(__FILE__) . "/card_" . $card['NAME'] . ".pdf");
//                                $fileId = CFile::SaveFile(
//                                    $arFile,
//                                    '',  // относительный путь от upload, где будут храниться файлы
//                                    false,    // ForceMD5
//                                    false     // SkipExt
//                                );

//                                $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
//								file_put_contents('/home/user/cards.metro-cc.ru/www/img_shrih_kod/' . $card['NAME'] . '.png',
//                                    				$generator->getBarcode($card['NAME'], $generator::TYPE_INTERLEAVED_2_5));
                                \Local\Cards::generateShrihKod($arResult);

								//var_dump($file_check);
								//var_dump($card['NAME']);

                                if(empty($arParams['isManager'])) {
//file_put_contents($_SERVER['DOCUMENT_ROOT'].'/pdf.log', PHP_EOL.'empty case sendMail',FILE_APPEND);
                                    \Local\Helpers\Applications::sendMail($arResult);
                                } else {
//file_put_contents($_SERVER['DOCUMENT_ROOT'].'/pdf.log', PHP_EOL.'NOT empty case sendUserUpdateMail',FILE_APPEND);
                                    \Local\Helpers\Applications::sendUserUpdateMail($orderId, $arResult);
                                }
//                                $arEventFields = array(
//                                    'NUM_CART' => $card['NAME'],
//                                    'CLIENT' =>  \Local\Helpers\Applications::getTreatment($arResult),
//                                    'EMAIL' => $arResult['email'],
//                                    'SHRIH_KOD' => $card['NAME'] . '.png',
//                                );
//                                $event = new \CEvent;
//
//                                $event->SendImmediate("INVAIT_KARTA", 's1', $arEventFields, "N", 8, array($fileId));
//                                //$event->Send("INVAIT_KARTA", 's1', $arEventFields, "N", 8,$arFile);
//                                CFile::Delete($fileId); // удаляем файл
//                                preExit($arResult);
//file_put_contents($_SERVER['DOCUMENT_ROOT'].'/pdf.log', PHP_EOL.'going to redirect... finish.',FILE_APPEND);
								LocalRedirect('/thanks' . (empty($arParams['isManager']) ? '' : '?manager='. $arParams['isManager']));

                            } 
                        } else {
                            $arResult['ERRORS_FIELD'][] = 'Ошибка при формировании формы pdf. ';
                            $errors = true;
                        }
                    } else {
                        $arFilter = [
                            'IBLOCK_ID' => Constants::$IBLOCK_PARTNERS,
                            'ID' => $partnerID
                        ];
                        $arSelect = Array("ID", "NAME", "PROPERTY_E_MAIL");

                        $partners = $ibElement->GetList([], $arFilter, false, $arSelect);
                        while ($ar = $partners->GetNext()) {
                            $partner_name = $ar["NAME"];

                            $db_props = $ibElement->GetProperty(Constants::$IBLOCK_PARTNERS, $ar["ID"],
                                array("sort" => "asc"), Array("CODE" => "E_MAIL"));
                            if ($ar_props = $db_props->Fetch()) {
                                $partner_email = $ar_props["VALUE"];
                            } else {
                                $partner_email = '';
                            } //Дефолтовое мыло
                        }

                        if(empty($arParams['ID']) or isset($arParams['isEmptyCard'])) {
                            $arEventFields_Error = array(
                                'BASE_NAME'    => $partner_name,
                                'NAME'         => $arResult['fname'],
                                'MID_NAME'     => $arResult['mname'],
                                'LAST_NAME'    => $arResult['lname'],
                                'SEX'          => $arResult['sex'],
                                'BIRTHDAY'     => $DB->FormatDate($arResult['birthday'], "DD/MM/YYYY", "DD.MM.YYYY"),
                                'POSTCODE'     => $arResult['postcode'],
                                'REGION'       => $arResult['region'],
                                'DISTRICT'     => $arResult['district'],
                                'CITY'         => $arResult['city'],
                                'STREET'       => $arResult['street'],
                                'NUMBER_HOUSE' => $arResult['number-house'],
                                'HOUSING'      => $arResult['housing'],
                                'NUMBER_FLAT'  => $arResult['number-flat'],
                                'EMAIL' => $partner_email
                            );
                            if($isCardLoyalty) {
                                $arEventFields_Error['CARD_LOYALTY'] = $arResult['card_loyalty'];
                            }
                            $event_warning = new CEvent;
                            $event_warning->SendImmediate("OUT_OF_CARDS", 's1', $arEventFields_Error, "N", 11);

                            $arResult['ERRORS_FIELD'][] = 'Извините, закончились карты. Но мы оповестили администратора и передали ваши данные, письмо с картой будет в ближайшее время.';
                            $errors = true;
                        }

                        $arrPrp = [
                            'LAST_NAME' => $arResult['lname'],
                            'PARTNER' => $partnerID,
//                            'CARD' => '',
                            'EMAIL' => $arResult['email'],
                            'NAME' => $arResult['fname'],
                            'MID_NAME' => $arResult['mname'],
                            'SEX' => $arResult['sex'],
                            'BIRTHDAY' => $DB->FormatDate($arResult['birthday'], "DD/MM/YYYY", "DD.MM.YYYY"),
                            'POSTCODE' => $arResult['postcode'],
                            'REGION' => $arResult['region'],
                            'DISTRICT' => $arResult['district'],
                            'CITY' => $arResult['city'],
                            'STREET' => $arResult['street'],
                            'NUMBER_HOUSE' => $arResult['number-house'],
                            'HOUSING' => $arResult['housing'],
                            'NUMBER_FLAT' => $arResult['number-flat'],
                        ];
                        if($isCardLoyalty) {
                            $arrPrp['CARD_LOYALTY'] = $arResult['card_loyalty'];
                        }
                        $arrFields = [
                            'IBLOCK_ID' => Constants::$IBLOCK_APPLICATIONS,
                            'NAME' => $arResult['email'],
                            'ACTIVE' => 'N',
                            'PROPERTY_VALUES' => $arrPrp
                        ];
                        if(empty($arParams['ID'])) {
                            $res = $ibElement->Add($arrFields);
                        } else {
                            foreach ($arrFields['PROPERTY_VALUES'] as $key => $value) {
                                CIBlockElement::SetPropertyValues($arParams['ID'], Constants::$IBLOCK_APPLICATIONS, $value, $key);
                            }
                            $ibElement->Update($arItem['ID'], ['CODE' => '']);
                            if(empty($arResult['ERRORS_FIELD'])) {
                                foreach (\Local\Helpers\Applications::getDataPgf(array('ID' => $ID)) as $arItem) {
                                    $res = \Local\Cards::generatePdf($arItem);
                                    if($res) {
                                        \Local\Cards::generateShrihKod($arItem);
                                        \Local\Helpers\Applications::sendMail($arItem);
                                    }
                                    break;
                                }
                                LocalRedirect('/thanks' . (empty($arParams['isManager']) ? '' : '?manager='. $arParams['isManager']));
                            }
                        }
                    }
                }
			}

		}

		$arResult['sex_man']   = $sex_man;
		$arResult['sex_woman'] = $sex_woman;
	}
    $arResult['isCardLoyalty'] = $isCardLoyalty;

} else {
     $arResult['ERRORS_FIELD'][] = 'Не указан партнер в профиле пользователя';
}

$this->IncludeComponentTemplate();
